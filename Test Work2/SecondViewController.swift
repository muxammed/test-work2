//
//  SecondViewController.swift
//  Test Work2
//
//  Created by muxammed on 22.03.2020.
//  Copyright © 2020 peydalyzatlar. All rights reserved.
//

import UIKit

class SecondViewController:UIViewController{
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController!.isNavigationBarHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.init(red: 236/255, green: 239/255, blue: 245/255, alpha: 1)
       
    }
    
}
