//
//  Extensions.swift
//  Test Work2
//
//  Created by muxammed on 22.03.2020.
//  Copyright © 2020 peydalyzatlar. All rights reserved.
//

import Foundation
import UIKit


extension UIView{
    func addConsWithFormat(format:String, views: UIView...){
        var viewDic = [String:UIView]()
        for (index,view) in views.enumerated() {
            let key = "v\(index)"
            view.translatesAutoresizingMaskIntoConstraints = false
            viewDic[key] = view
        }
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutConstraint.FormatOptions(), metrics: nil, views: viewDic))
        
    }
    
    
}
