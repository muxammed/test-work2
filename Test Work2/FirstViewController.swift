//
//  ViewController.swift
//  Test Work2
//
//  Created by muxammed on 22.03.2020.
//  Copyright © 2020 peydalyzatlar. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController,UITextViewDelegate {
    
    let labelColor:UIColor = UIColor.init(red: 136/255, green: 136/255, blue: 136/255, alpha: 1)
    let labelWarnColor:UIColor = UIColor.init(red: 237/255, green: 124/255, blue: 86/255, alpha: 1)
    
    let toastBackgroundColor:UIColor = UIColor.init(red: 237/255, green: 124/255, blue: 86/255, alpha: 1)
    
    
    let labelOne:UILabel={
        let label = UILabel()
        label.text = "Регистрация"
        label.textAlignment = .center
        label.font = UIFont.init(name: "ArialRoundedMTProCyr-Bold", size: 30)
        return label
    }()
    
    let toastBackView:UIView = {
        let v = UIView()
        v.backgroundColor = UIColor.init(red: 237/255, green: 124/255, blue: 86/255, alpha: 1)
        return v
    }()
    
    let toastText:UILabel = {
        let lb = UILabel()
        lb.text = """
                    Пользователь с таким e-mail
                    уже зарегистрирован
                    """
        lb.textAlignment = .center
        lb.textColor = UIColor.white
        lb.numberOfLines = 2
        lb.font = UIFont.init(name: "ArialRoundedMTProCyr-Bold", size: 20)
        return lb
    }()
    
    let innerView:UIView={
        let v = UIView()
        v.backgroundColor = UIColor.white
        return v
    }()
    
    let borderView:UIView = {
        let bv = UIView()
        bv.layer.cornerRadius = 35
        bv.layer.borderWidth = 2
        bv.layer.borderColor = UIColor.init(red: 236/255, green: 239/255, blue: 245/255, alpha: 1).cgColor
        return bv
    }()
    
    let labelName:UILabel = {
        let lb = UILabel()
        lb.textColor = UIColor.init(red: 136/255, green: 136/255, blue: 136/255, alpha: 1)
        lb.text = "e-mail"
        lb.font = UIFont.init(name: "ArialRoundedMTProCyr-Bold", size: 17)
        lb.textAlignment = .center
        return lb
    }()
    
    let emailText:UITextView = {
        let tv = UITextView()
        tv.font = UIFont.init(name: "ArialRoundedMTProCyr-Bold", size: 17)
        tv.textAlignment = .center
//        tv.backgroundColor = .gray
        return tv
    }()
    
    let borderView2:UIView = {
        let bv = UIView()
        bv.layer.cornerRadius = 35
        bv.layer.borderWidth = 2
        bv.layer.borderColor = UIColor.init(red: 236/255, green: 239/255, blue: 245/255, alpha: 1).cgColor
        return bv
    }()
    
    let borderView3:UIView = {
        let bv = UIView()
        bv.layer.cornerRadius = 35
        bv.layer.borderWidth = 2
        bv.layer.borderColor = UIColor.init(red: 236/255, green: 239/255, blue: 245/255, alpha: 1).cgColor
        return bv
    }()
    
    let regButton:UIButton = {
        let btn = UIButton()
        btn.backgroundColor =  UIColor.init(red: 236/255, green: 239/255, blue: 245/255, alpha: 1)
        btn.setTitle("ЗАРЕГИСТРИРОВАТЬСЯ", for: .normal)
        btn.setTitleColor(UIColor.init(red: 69/255, green: 206/255, blue: 192/255, alpha: 1), for: .normal)
        btn.layer.cornerRadius = 30
        btn.titleLabel?.font = UIFont.init(name: "ArialRoundedMTProCyr-Bold", size: 17);
        return btn
    }()
    
    let fbButton:UIView = {
        let v = UIView()
        v.backgroundColor = UIColor.init(red: 236/255, green: 239/255, blue: 245/255, alpha: 1)
        v.layer.cornerRadius = 30
        return v
    }()
    
    let vkButton:UIView = {
        let v = UIView()
        v.backgroundColor = UIColor.init(red: 236/255, green: 239/255, blue: 245/255, alpha: 1)
        v.layer.cornerRadius = 30
        return v
    }()
    
    let fbImage:UIImageView = {
        let img = UIImageView()
        img.image = UIImage(named: "facebook")
        img.contentMode = UIImageView.ContentMode.scaleAspectFit
        return img
    }()
    
    let vkImage:UIImageView = {
        let img = UIImageView()
        img.image = UIImage(named: "vk")
        img.contentMode = UIImageView.ContentMode.scaleAspectFit
        return img
       }()
    
    let validImage:UIImageView = {
        let im = UIImageView()
        im.image = UIImage.init(named: "valid")
        return im
    }()
    
    let notValidImage:UIImageView = {
        let im = UIImageView()
        im.image = UIImage.init(named: "notvalid")
        return im
    }()
    
    let vBottom = UIView()

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController!.isNavigationBarHidden = true
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        
        setupViews()
        // Do any additional setup after loading the view.
    }
    
    //Constraints to animate
    var labelNameXConstraint:NSLayoutConstraint?
    var labelNameWidthConstraint:NSLayoutConstraint?
    var labelNameHeigthConstraint:NSLayoutConstraint?
    
    var emailTextXConstraint:NSLayoutConstraint?
    var emailTextWidthConstraint:NSLayoutConstraint?
    var emailTextHeigthConstraint:NSLayoutConstraint?
    var emailTextYConstraint:NSLayoutConstraint?
    
    var validXConstraint:NSLayoutConstraint?
    var validWidthConstraint:NSLayoutConstraint?
    var validHeigthConstraint:NSLayoutConstraint?
    var validYConstraint:NSLayoutConstraint?
    
    var notValidXConstraint:NSLayoutConstraint?
    var notValidWidthConstraint:NSLayoutConstraint?
    var notValidHeigthConstraint:NSLayoutConstraint?
    var notValidYConstraint:NSLayoutConstraint?
    
    var toastXConstraint:NSLayoutConstraint?
    var toastWidthConstraint:NSLayoutConstraint?
    var toastHeigthConstraint:NSLayoutConstraint?
    var toastYConstraint:NSLayoutConstraint?
    
    func setupViews(){
        
        
        innerView.addSubview(labelOne)
        borderView.addSubview(emailText)
        borderView.addSubview(labelName)
        borderView.addSubview(validImage)
        borderView.addSubview(notValidImage)
        innerView.addSubview(borderView)
        
        innerView.addSubview(borderView2)
        innerView.addSubview(borderView3)
        innerView.addSubview(regButton)
        innerView.addSubview(vBottom)
        fbButton.addSubview(fbImage)
        vkButton.addSubview(vkImage)
        vBottom.addSubview(fbButton)
        vBottom.addSubview(vkButton)
        
        
        view.addSubview(innerView)
        toastBackView.addSubview(toastText)
        view.addSubview(toastBackView)
        
        view.addConsWithFormat(format: "H:[v0(300)]", views: innerView,labelOne)
        view.addConsWithFormat(format: "V:[v0(>=v1)]", views: innerView,labelOne)
        
        toastBackView.addConsWithFormat(format: "H:|[v0]|", views: toastText)
        toastBackView.addConsWithFormat(format: "V:|[v0]|", views: toastText)
        toastBackView.addConstraint(NSLayoutConstraint.init(item: toastText, attribute: .centerY, relatedBy: .equal, toItem: toastBackView, attribute: .centerY, multiplier: 1, constant: 0))
        
        innerView.addConsWithFormat(format: "H:|[v0]|", views: labelOne)
        innerView.addConsWithFormat(format: "V:|-8-[v0]-30-[v1(70)]-8-[v2(70)]-8-[v3(70)]-8-[v4(60)]-80-[v5(60)]-8-|", views: labelOne,borderView,borderView2,borderView3,regButton,vBottom)
        
        labelName.translatesAutoresizingMaskIntoConstraints = false
        emailText.translatesAutoresizingMaskIntoConstraints = false
        validImage.translatesAutoresizingMaskIntoConstraints = false
        notValidImage.translatesAutoresizingMaskIntoConstraints = false
        toastBackView.translatesAutoresizingMaskIntoConstraints = false
        
        labelNameXConstraint = NSLayoutConstraint(item: labelName, attribute: .centerY, relatedBy: .equal, toItem: borderView, attribute: .centerY, multiplier: 1, constant: 0)
        labelNameXConstraint?.isActive = true
        labelNameWidthConstraint = NSLayoutConstraint(item: labelName, attribute: .width, relatedBy: .equal, toItem: borderView, attribute: .width, multiplier: 1, constant: 0)
        labelNameWidthConstraint?.isActive = true
        labelNameHeigthConstraint = NSLayoutConstraint(item: labelName, attribute: .height, relatedBy: .equal, toItem: borderView, attribute: .height, multiplier: 1, constant: 0)
        labelNameHeigthConstraint?.isActive = true
        
        emailTextXConstraint = NSLayoutConstraint(item: emailText, attribute: .centerY, relatedBy: .equal, toItem: borderView, attribute: .centerY, multiplier: 1, constant: 0)
        emailTextXConstraint?.isActive = true
        emailTextYConstraint = NSLayoutConstraint(item: emailText, attribute: .centerX, relatedBy: .equal, toItem: borderView, attribute: .centerX, multiplier: 1, constant: 0)
        emailTextYConstraint?.isActive = true
        emailTextWidthConstraint = NSLayoutConstraint(item: emailText, attribute: .width, relatedBy: .equal, toItem: labelName, attribute: .width, multiplier: 0, constant: 0)
        emailTextWidthConstraint?.isActive = true
        emailTextHeigthConstraint = NSLayoutConstraint(item: emailText, attribute: .height, relatedBy: .equal, toItem: labelName, attribute: .height, multiplier: 0, constant: 0)
        emailTextHeigthConstraint?.isActive = true
        
        validYConstraint = NSLayoutConstraint(item: validImage, attribute: .centerY, relatedBy: .equal, toItem: borderView, attribute: .centerY, multiplier: 1, constant: 0)
        validYConstraint?.isActive = true
        validXConstraint = NSLayoutConstraint(item: validImage, attribute: .trailing, relatedBy: .equal, toItem: borderView, attribute: .trailing, multiplier: 1, constant: -17.5)
        validXConstraint?.isActive = true
        validWidthConstraint = NSLayoutConstraint(item: validImage, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 0, constant: 0)
        validWidthConstraint?.isActive = true
        validHeigthConstraint = NSLayoutConstraint(item: validImage, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 0, constant: 0)
        validHeigthConstraint?.isActive = true
        
        notValidYConstraint = NSLayoutConstraint(item: notValidImage, attribute: .centerY, relatedBy: .equal, toItem: borderView, attribute: .centerY, multiplier: 1, constant: 0)
        notValidYConstraint?.isActive = true
        notValidXConstraint = NSLayoutConstraint(item: notValidImage, attribute: .trailing, relatedBy: .equal, toItem: borderView, attribute: .trailing, multiplier: 1, constant: -17.5)
        notValidXConstraint?.isActive = true
        notValidWidthConstraint = NSLayoutConstraint(item: notValidImage, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 0, constant: 0)
        notValidWidthConstraint?.isActive = true
        notValidHeigthConstraint = NSLayoutConstraint(item: notValidImage, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 0, constant: 0)
        notValidHeigthConstraint?.isActive = true
        
        toastYConstraint = NSLayoutConstraint(item: toastBackView, attribute: .top, relatedBy: .equal, toItem: view, attribute: .top, multiplier: 1, constant: -200)
        toastYConstraint?.isActive = true
        toastXConstraint = NSLayoutConstraint(item: toastBackView, attribute: .centerX, relatedBy: .equal, toItem: view, attribute: .centerX, multiplier: 1, constant: 0)
        toastXConstraint?.isActive = true
        toastWidthConstraint = NSLayoutConstraint(item: toastBackView, attribute: .width, relatedBy: .equal, toItem: view, attribute: .width, multiplier: 1, constant: 0)
        toastWidthConstraint?.isActive = true
        toastHeigthConstraint = NSLayoutConstraint(item: toastBackView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: 170)
        toastHeigthConstraint?.isActive = true
        
        
        
        innerView.addConsWithFormat(format: "H:|[v0]|", views: borderView)
        
        
        innerView.addConsWithFormat(format: "H:|[v0]|", views: borderView2)
        innerView.addConsWithFormat(format: "H:|[v0]|", views: borderView3)
        innerView.addConsWithFormat(format: "H:|[v0]|", views: regButton)
        innerView.addConsWithFormat(format: "H:|[v0]|", views: vBottom)
        vkButton.addConsWithFormat(format: "V:|-16-[v0]-16-|", views: vkImage)
        fbButton.addConsWithFormat(format: "V:|-16-[v0]-16-|", views: fbImage)
        
        vBottom.addConsWithFormat(format: "H:|[v0]-8-[v1(==v0)]|", views: fbButton,vkButton)
        
        vBottom.addConsWithFormat(format: "V:|[v0]|", views: fbButton)
        vBottom.addConsWithFormat(format: "V:|[v0]|", views: vkButton)
        
        
        
        view.addConstraint(NSLayoutConstraint.init(item: innerView, attribute: .centerX, relatedBy: .equal, toItem: view, attribute: .centerX, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint.init(item: innerView, attribute: .centerY, relatedBy: .equal, toItem: view, attribute: .centerY, multiplier: 1, constant: 0))
        
        fbButton.addConstraint(NSLayoutConstraint.init(item: fbImage, attribute: .centerX, relatedBy: .equal, toItem: fbButton, attribute: .centerX, multiplier: 1, constant: 0))
        vkButton.addConstraint(NSLayoutConstraint.init(item: vkImage, attribute: .centerX, relatedBy: .equal, toItem: vkButton, attribute: .centerX, multiplier: 1, constant: 0))
        
        
        regButton.addTarget(self, action: #selector(regAction), for: .touchUpInside)
        let emailTouch = UITapGestureRecognizer(target: self, action: #selector(emailTapAction))
        borderView.addGestureRecognizer(emailTouch)
        
        let backTap = UITapGestureRecognizer(target: self, action: #selector(dissmisAllAction))
        self.view.addGestureRecognizer(backTap)
    }

    
    @objc func dissmisAllAction(){
        
        if (self.emailText.text.count == 0 && self.emailText.isFirstResponder){
            emailTapAction()
        }
        afterValidate()
        self.view.endEditing(true)
    }
    
    @objc func emailTapAction(){
        
        UIView.animate(withDuration: 0.25, delay: 0.0, options: [.curveEaseOut], animations: {

            if (Int(self.emailTextWidthConstraint!.constant) > 0){
                self.labelNameHeigthConstraint?.constant = 70
                self.labelNameXConstraint?.constant = 0
                self.labelName.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                
                self.emailTextHeigthConstraint?.constant = 0
                self.emailTextWidthConstraint?.constant = 0
                self.emailTextXConstraint?.constant = 0
                self.view.endEditing(true)
            } else {

                self.labelNameHeigthConstraint?.constant = 25
                self.labelNameXConstraint?.constant = -17.5
                self.labelName.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
                
                self.emailTextHeigthConstraint?.constant = 45
                self.emailTextWidthConstraint?.constant = 230
                self.emailTextXConstraint?.constant = 17.5
                self.emailText.becomeFirstResponder()
                
            }
            
            
            
            
            self.view.layoutIfNeeded()
        },completion: nil)
    }
    
    @objc func regAction(){
        
        //make validation
        
       
        //afterValidate()
        if (!valid()){
            print("not valid")
            UIView.animate(withDuration: 0.25, delay: 0.0, options: [.curveEaseOut], animations: {
                self.toastYConstraint?.constant = 0
                self.view.layoutIfNeeded()
            }) { (oldumy) in
                UIView.animate(withDuration: 0.25, delay: 1.5,options: [.curveEaseIn] , animations: {
                    self.toastYConstraint?.constant = -170
                    self.view.layoutIfNeeded()
                }, completion: nil)
            }
        } else {
            self.navigationController?.pushViewController(SecondViewController(), animated: true)
        }
        
    
        
    }
    
    func afterValidate(){
        if (valid()){
            
            UIView.animate(withDuration: 0.25, delay: 0.0, options: [.curveEaseOut], animations: {
                        self.validHeigthConstraint?.constant = 30
                        self.validWidthConstraint?.constant = 30
                        self.notValidHeigthConstraint?.constant = 0
                        self.notValidWidthConstraint?.constant = 0
                       self.view.layoutIfNeeded()
                   },completion: nil)
            
            self.borderView.layer.borderColor = UIColor.init(red: 236/255, green: 239/255, blue: 245/255, alpha: 1).cgColor
            self.emailText.textColor = UIColor.black
            
        } else {
            UIView.animate(withDuration: 0.25, delay: 0.0, options: [.curveEaseOut], animations: {
                        self.notValidHeigthConstraint?.constant = 30
                        self.notValidWidthConstraint?.constant = 30
                        self.validHeigthConstraint?.constant = 0
                        self.validWidthConstraint?.constant = 0
                       self.view.layoutIfNeeded()
                   },completion: nil)
            
            self.borderView.layer.borderColor = UIColor.init(red: 237/255, green: 124/255, blue: 86/255, alpha: 1).cgColor
            self.emailText.textColor = UIColor.init(red: 237/255, green: 124/255, blue: 86/255, alpha: 1)
        }
    }
    
    func valid() -> Bool {
        
        let email = self.emailText.text ?? ""
        if (email.count > 0){
            if (email.contains("@")){
                return true
            }
        }
        return false
    }
    
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
           print("it's work")
           return true
       }

}
//
//extension FirstViewController:UITextViewDelegate{
//
//    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
//        print("it's work")
//        return true
//    }
//}
